Computer code to construct all graphs and tables in *“Optimal combination of forecasts with mean absolute error loss”* by **Felix Chan** and **Laurent Pauwels**. 

The folder structure follows the order of sections in the paper:

**Section 2 Motivation**:

*TheoryGraphs.ipynb*: Jupyter Notebook to construct the graphs featured in Figure 1 of Section 2 of the paper. The framework with which those graphs are constructed is explained in the Jupyter Notebook. The Kernel used is Julia 1.6.1.



**Section 4 Simulations**: 

The simulations presented in the paper are conducted with MatLab and Julia. The computer code is available in both languages. The Julia Language code is presented in a Jupyter Notebook. There may be variations in the results because of differences in the random number generator and the solver (optimizer) in the two software.

*SN_Set1.mat* and *SN_Set2.mat*: MatLab mat files containing the exact values for b (Skewness Parameter) and Sigma (Random covariance matrix) for 2 different sets of optimal weights, a. 

*SimulationsJulia.ipynb*: Jupyter Notebook with Julia Language to conduct the simulations found in Section 4. It is also replicated in the MatLab code found in *Simulations.m* and *maeloss.m*. The Kernel used is Julia 1.6.1.



**Section 5 Illustration**:

The empirical analysis presented in the paper are conducted with MatLab and Julia. The computer code is available in both languages. The Julia Language code is presented in a Jupyter Notebook. There may be variations in the results because of differences in the solver (optimizer) in the two software.

Datasets: *InflationData.csv*, *GrowthData.csv*, and *UnempData.csv* are the datasets for inflation rate, real growth rate, and unemployment rate in .csv format.

*EmpiricsJulia.ipynb*: Jupyter Notebook with Julia Language to conduct the empirical study found in Section 5. It is also replicated in the MatLab code found in *Empirics.m* and *maeloss.m*. The Kernel used is Julia 1.6.1.



**MatLab Code**:

*Simulations.m*: MatLab script that conducts the simulations found in Section 4 of the paper. 

*Empirics.m*: MatLab script that conducts the empirical study found in Section 5 of the paper. 

*maeloss.m*: MatLab function that computes mean absolute error loss.

Datasets: *InflationData.mat*, *GrowthData.mat*, and *UnempData.mat* are the datasets for inflation rate, real growth rate, and unemployment rate in .mat (MatLab) format.

*SN_Set1.mat* and *SN_Set2.mat*: MatLab mat files containing the exact values for b (Skewness Parameter) and Sigma (Random covariance matrix) for 2 different sets of optimal weights, a. (Needed for the simulations)